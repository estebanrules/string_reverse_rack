# Write a rack app that uses Rack::Request and Rack::Response to reverse a string
# passed to the app by a client.
class StringReverse
  def call(env)
    req = Rack::Request.new(env)
    string = req.params['string']

    Rack::Response.new.finish do |res|
      res['Content-Type'] = 'text-plain'
      res.status = 200
      str = "Your reversed string: #{string.reverse}"
      res.write str
    end
  end
end



